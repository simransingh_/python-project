# Ask user for message
print("Type een bericht:")
message = input()

#ask user for key
print("Kies een sleutel:")
key = input()

#create a range with the length of the message
ran = range(len(message))

encryptedMessage = ""

# Iterate through the range and therefor show all letters
for i in ran:
    currentLetter = message[i]
    currentKeyLetter = key[i % len(key)]
    numberLetter = ord(currentLetter)
    numberKeyLetter = ord(currentKeyLetter)

    # For encryption: add (step to right)
    # for decryption: subtract (step to left)

    sumOfTwoLetters = numberLetter + numberKeyLetter
    
    newNumberLetter = sumOfTwoLetters
    sumOfTwoLetters = sumOfTwoLetters %128
    #get the encrypted letter based on number
    newLetter = chr(newNumberLetter)

    #print out the result
    printText = currentLetter + "(" + str(numberLetter) + " ) + "
    printText += currentKeyLetter + "(" + str(numberKeyLetter) + ") = "
    printText += newLetter + "(" + str(newNumberLetter) + ")"
    print(printText)
input('druk ENTER om af te sluiten')
